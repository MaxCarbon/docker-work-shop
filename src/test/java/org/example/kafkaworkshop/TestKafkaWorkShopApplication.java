package org.example.kafkaworkshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration(proxyBeanMethods = false)
public class TestKafkaWorkShopApplication {

    public static void main(String[] args) {
        SpringApplication.from(KafkaWorkShopApplication::main).with(TestKafkaWorkShopApplication.class).run(args);
    }

}
