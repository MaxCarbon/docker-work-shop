package org.example.kafkaworkshop.controller;


import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.kafkaworkshop.service.KafkaService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Slf4j
@Controller
@AllArgsConstructor
public class KafkaController {

    private KafkaService kafkaService;

    @PostMapping("/post-message")
    public ResponseEntity<String> postMessage(@RequestBody() String message) {
        var send = kafkaService.send(message);

        return ResponseEntity.ok(send);
    }

}
