package org.example.kafkaworkshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaWorkShopApplication {

    public static void main(String[] args) {
        SpringApplication.run(KafkaWorkShopApplication.class, args);
    }

}
