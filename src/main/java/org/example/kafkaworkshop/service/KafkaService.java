package org.example.kafkaworkshop.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Slf4j
@Service
@AllArgsConstructor
public class KafkaService {

    private ClientMessageSender clientMessageSender;

    public String send(String message){
        CompletableFuture<SendResult<String, String>> send = clientMessageSender.sendMessage("topic-work-shop", message);
        send.whenComplete((result, ex) -> {
            if (ex != null) {
                log.error(MessageFormat.format("FAILURE Result : {0} ", ex.getMessage()), ex);
            } else {
                log.debug("SUCCESS Result : {}", result.getRecordMetadata());
            }
        });
        return "ok";
    }

}
