package org.example.kafkaworkshop.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;

@Slf4j
@Service
@AllArgsConstructor
public class ICPConsumerService {

    //TODO : implement the consumer with KafkaListener
    public void consume(ConsumerRecord<String, String> message) {
        log.info(MessageFormat.format("message reçu sur le topic {0}", message.topic()));
        log.info(MessageFormat.format("message reçu : {0} ", message.value()));
    }
}